A logging library for bash designed to mimic the behavior of log4j or logger in Java.

Log levels supported:

 * DEBUG
 * INFO
 * WARN
 * ERROR
 * SEVERE 

Usage:

```
source ./bashlogger/src/bashlogger.sh
setloglevel LEVEL
logger LEVEL "message"
```
