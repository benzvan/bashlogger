#!/usr/bin/env bash

tests=0
failures=0
passes=0
work=/tmp/${USER}_work
result="Result:\n"

if [ -d "${work}" ]; then
    echo "please rm -rf $work"
    exit 1
else
    mkdir -p "${work}"
    [[ -d ${work} ]] || exit 2
fi

source ../src/bashlogger.sh

# Test - getloginteger 
((tests++))
if [ "$(getloginteger SEVERE)" == 0 ]; then
    ((passes++))
    result="${result} Assert getloginteger SEVERE == 0 passed\n"
else
    ((failures++))
    result="${result} Assert getloginteger SEVERE == 0 failed\n"
fi

((tests++))
if [ "$(getloginteger DEBUG)" == 4 ]; then
    ((passes++))
    result="${result} Assert getloginteger DEBUG == 4 passed\n"
else
    ((failures++))
    result="${result} Assert getloginteger DEBUG == 4 failed\n"
fi

# Test - getlogstring
((tests++))
if [ "$(getlogstring 0)" == "SEVERE" ]; then
	((passes++))
	result="${result} Assert getlogstring 0 == SEVERE passed\n"
else
	((failures++))
	result="${result} Assert getlogstring 0 == SEVERE failed\n"
fi

# test - output destinations
setloglevel TRACE

logger SEVERE "severe message" >> "${work}"/stdout 2>> "${work}"/stderr
logger ERROR "error message" >> "${work}"/stdout 2>> "${work}"/stderr
logger WARN "warn message" >> "${work}"/stdout 2>> "${work}"/stderr
logger INFO "info message" >> "${work}"/stdout 2>> "${work}"/stderr
logger DEBUG "debug message" >> "${work}"/stdout 2>> "${work}"/stderr
logger TRACE "trace message" >> "${work}"/stdout 2>> "${work}"/stderr

((tests++))
if grep -q SEVERE "${work}"/stdout; then
    ((failures++))
    result="${result} Assert SEVERE does not go to stdout failed\n"
else
    ((passes++))
    result="${result} Assert SEVERE does not go to stdout passed\n"
fi

((tests++))
if grep -q SEVERE "${work}"/stderr; then
    ((passes++))
    result="${result} Assert SEVERE goes to stderr passed\n"
else
    ((failures++))
    result="${result} Assert SEVERE goes to stderr failed\n"
fi

((tests++))
if grep -q INFO "${work}"/stderr; then
    ((failures++))
    result="${result} Assert INFO does not go to stderr failed\n"
else
    ((passes++))
    result="${result} Assert INFO does not go to stderr passed\n"
fi

((tests++))
if grep -q INFO "${work}"/stdout; then
    ((passes++))
    result="${result} Assert INFO goes to stdout passed\n"
else
    ((failures++))
    result="${result} Assert INFO goes to stdout failed\n"
fi

# test - get log level
setloglevel TRACE
GET_LOG_LEVEL=$(getloglevel)
((tests++))
if [[ "${GET_LOG_LEVEL}" == "TRACE" ]]; then
	((passes++))
	result="${result} Assert getloglevel returns setloglevel passed\n"
else
	((failures++))
	result="${result} Assert getloglevel returns setloglevel failed\n"
	result="${result}	${GET_LOG_LEVEL} != TRACE\n"
fi

echo -e $result
echo "Summary:"
echo "Tests: ${tests}, Passed: ${passes}, Failed: ${failures}"

rm -rf "${work}"
