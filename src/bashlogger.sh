#!/usr/bin/env bash
# A logging library for bash designed to mimic the behavior of log4j or logger in Java.
# https://gitlab.com/benzvan/bashlogger

LEVELS=('SEVERE' 'ERROR' 'WARN' 'INFO' 'DEBUG' 'TRACE')

function getloginteger {
  for i in "${!LEVELS[@]}"; do
     if [[ "${LEVELS[$i]}" = "${1}" ]]; then
         echo "${i}"
     fi
  done
}

function setloglevel {
  LOGLEVEL=$(getloginteger "${1}")
}

function getloglevel {
  getlogstring "${LOGLEVEL}"
}

function getlogstring {
	echo "${LEVELS[$1]}"
}

function logger {
  if [ "$(getloginteger "${1}")" -le "${LOGLEVEL}" ];then
    if [ "$(getloginteger "${1}")" -le "$(getloginteger ERROR)" ];then
      echo -e "$1: $2" 1>&2
    else
      echo -e "${1}: $2"
    fi
  fi
}

#Default log level is INFO
setloglevel INFO

